interface Task {
    task: string,
    id: number
}

class API {
    allTodos: Task[] = [];

    addTask(task: string): void {
        if (task === undefined || task === "") return;
        const todo: Task = {
            task: task,
            id: Date.now()
        };

        this.allTodos.push(todo);
        this.saveTasks(this.allTodos);
    }

    deleteTask(taskID: number): void {
        this.allTodos.splice(taskID, 1);
        this.saveTasks(this.allTodos)
    }
    // "build": "parcel build src/index.html"


    get getTasks(): Task[] {
        return this.allTodos;
    }

    saveTasks(allTodos: Task[]): void {
        const save: string = JSON.stringify(allTodos);
        localStorage.setItem('todos', save);
    }

}

class App {
    API: API = new API();
    appNewTaskInput = document.querySelector('#addTaskText') as HTMLInputElement;
    appNewTaskAdd = document.querySelector('#addTaskButton') as HTMLButtonElement;
    appTaskList = document.querySelector('.todoApp__list') as Element;

    constructor() {

        this.appNewTaskAdd.onclick = (e => {
            this.addTask(this.appNewTaskInput.value);
        });

        this.appNewTaskInput.onclick = (e => {
            this.addTask(this.appNewTaskInput.value);
        });

        this.renderTasks();

    }

    addTask(task: string): void {
        if (task === undefined || task === "") return;

        this.API.addTask(task);
        this.appNewTaskInput.value = "";
        this.renderTasks();
    }


    renderTasks(): void {
        this.appTaskList.innerHTML = "";

        const allTasks = this.API.getTasks;
        allTasks.forEach((task, index) => {
            let template = `<div class="todoApp__todo" data-task-id="${index}">
      <label class="todo__label"> ${task.task} </label>
      <div class="todo__actions">
      <button class="todo__action-delete">Delete</button>
      </div>
      </div>`;
            this.appTaskList.innerHTML += template;
        });
        this.eventListener();
    }


    eventListener(): void {
        const deleteButton = document.querySelectorAll('.todoApp__todo .todo__action-delete');
        const allTasks = this.API.allTodos;


        deleteButton.forEach(dlt => {
            dlt.addEventListener('click', (e) => {
                allTasks.forEach((task, index) => {
                    allTasks[index].id === task.id ? this.API.deleteTask(index) : null
                })
                this.renderTasks();
            })
        })
    }


}

const app = new App();
